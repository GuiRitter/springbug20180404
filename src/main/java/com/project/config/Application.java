package com.project.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

@SpringBootApplication
@ComponentScan("com.project")
@EntityScan(basePackages = { "com.project.model" })
@EnableJpaRepositories("com.project.repository")
public class Application extends RepositoryRestConfigurerAdapter {

	@Autowired
	private ValidatorArray validatorArray;

	/**
	 * http://www.baeldung.com/spring-data-rest-validators
	 * 2.4.  Event Discovery Bug
	 */
	@Override
	public void configureValidatingRepositoryEventListener(ValidatingRepositoryEventListener validatingListener) {

		super.configureValidatingRepositoryEventListener(validatingListener);

		for (Validator validator : validatorArray.getValidatorArray()) {

			for (String validateMethod : validatorArray.getValidateMethodArray()) {

				if ((validateMethod == ValidatorArray.BEFORE_CREATE) && (validator instanceof /* ... */)) {
					continue;
				}
				validatingListener.addValidator(validateMethod, validator);
			}
		}
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
