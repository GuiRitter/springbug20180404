package com.project.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Validator;

// ...
import com.project.validator.SaleItemValidator;
// ...

public class ValidatorArray {

	// ...

	@Autowired
	private SaleItemValidator saleItemValidator;

	// ...

	private Validator validatorArray[] = null;

	public static final String BEFORE_CREATE   = "beforeCreate"  ;
	public static final String AFTER_CREATE    = "afterCreate"   ;
	public static final String BEFORE_SAVE     = "beforeSave"    ;
	public static final String AFTER_SAVE      = "afterSave"     ;
	public static final String BEFORE_LINKSAVE = "beforeLinkSave";
	public static final String AFTER_LINKSAVE  = "afterLinkSave" ;
	public static final String BEFORE_DELETE   = "beforeDelete"  ;
	public static final String AFTER_DELETE    = "afterDelete"   ;

	private String[] validateMethodArray = new String[]{
			BEFORE_CREATE  ,
			AFTER_CREATE   ,
			BEFORE_SAVE    ,
			AFTER_SAVE     ,
			BEFORE_LINKSAVE,
			AFTER_LINKSAVE ,
			BEFORE_DELETE  ,
			AFTER_DELETE   };

	public String[] getValidateMethodArray() {
		return validateMethodArray;
	}

	public Validator[] getValidatorArray() {
		return validatorArray;
	}

	public void init() {
		validatorArray = new Validator[]{
				// ...
				itemVendaValidator,
				// ...
				};
	}
}
