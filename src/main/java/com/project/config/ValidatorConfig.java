package com.project.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

// ...
import com.project.validator.SaleItemValidator;
// ...

@Configuration
public class ValidatorConfig {

	// ...

	@Bean
	public SaleItemValidator saleItemValidator() {
		return new SaleItemValidator();
	}

	// ...

	@Bean(initMethod = "init")
	public ValidatorArray validatorArray() {
		return new ValidatorArray();
	}
}
