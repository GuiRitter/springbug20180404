package com.project.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.project.model.common.RegistryMultiCompany;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@Entity
@Table(name="product_group")
@AttributeOverrides( { @AttributeOverride(name = "id", column = @Column(name = "id_product_group")) } )
public class ProductGroup extends RegistryMultiCompany {

	@ManyToOne(cascade = { CascadeType.MERGE })
	@JoinTable(name="product_group_subgroup",
	joinColumns={@JoinColumn(name="id_subgroup_product")}, inverseJoinColumns={@JoinColumn(name="id_group_product")})
//	@JsonBackReference
	private ProductGroup parentGroup;

	@OneToMany(cascade = { CascadeType.MERGE }, mappedBy="parentGroup")
//	@JsonManagedReference
	private List<ProductGroup> subgroupList = new ArrayList<ProductGroup>();

	// Necessary for Hibernate
	protected ProductGroup() {}

	public ProductGroup(Company company, String code, String name) {
		this.addCompany(company);
		this.code = code;
		this.name = name;
	}

	public void addSubgroup(ProductGroup subgroup) {
		subgroup.setParentGroup(this);
		this.subgroupList.add(subgroup);
	}

	public List<ProductGroup> getSubgroupList() {
		return Collections.unmodifiableList(this.subgroupList);
	}
}
