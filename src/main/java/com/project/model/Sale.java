package com.project.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.project.model.common.RegistrySingleCompany;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false, exclude={"saleItemList", /* ... */})
@Entity
@Table(name="sale")
@AttributeOverrides( { @AttributeOverride(name = "id", column = @Column(name = "id_sale")) } )
//@JsonIdentityInfo(
//		  generator = ObjectIdGenerators.PropertyGenerator.class,
//		  property = "id",
//		  scope = Sale.class)
public class Sale extends RegistrySingleCompany {

	// ...

//	@JsonManagedReference
	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy="sale")
	private List<SaleItem> saleItemList = new ArrayList<>();

	// ...

	// Necessary for Hibernate
	protected Sale() {}

	public Sale(/* ... */) {
		// ...
	}

	@JsonCreator
	protected Sale(
			@JsonProperty("id") Long id,
			@JsonProperty("isDeleted") Boolean isDeleted,
			@JsonProperty("isEnabled") Boolean isEnabled,
			@JsonProperty("company") Company company,
			@JsonProperty("saleItemList") List<SaleItem> saleItemList,
			/* ... */) {
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public List<SaleItem> getSaleItemList() {
		return Collections.unmodifiableList(saleItemList);
	}

	// ...
}
