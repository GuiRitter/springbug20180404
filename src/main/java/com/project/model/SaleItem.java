package com.project.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.project.model.common.RegistrySingleCompany;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@Entity
@Table(name="sale_item")
@AttributeOverrides( { @AttributeOverride(name = "id", column = @Column(name = "id_sale_item")) } )
//@JsonIdentityInfo(
//		  generator = ObjectIdGenerators.PropertyGenerator.class,
//		  property = "id",
//		  scope = SaleItem.class)
@SaleItemConstraint
public class SaleItem extends RegistrySingleCompany {

	// ...

//	@JsonBackReference
	@ManyToOne
	@JoinColumn(name="id_sale", nullable = false)
	private Sale sale;

	public SaleItem(/* ... */) {
		// ...
	}

	@JsonCreator
	protected SaleItem(
			@JsonProperty("id") Long id,
			@JsonProperty("isDeleted") Boolean isDeleted,
			@JsonProperty("isEnabled") Boolean isEnabled,
			@JsonProperty("company") Company company,
			@JsonProperty("sale") Sale sale,
			/* ... */) {
	}

	public SaleItem() {}

	// ...
}
