package com.project.model.common;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotBlank;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@MappedSuperclass
public abstract class Registry extends Record {

    @NotBlank
    @Column(name = "code", length = 15)
    protected String code;

    @NotBlank
    @Column(name = "name", length = 40)
    protected String name;
}