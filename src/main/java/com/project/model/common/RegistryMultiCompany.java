package com.project.model.common;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.project.model.Company;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@MappedSuperclass
public class RegistryMultiCompany extends Registry {

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE })
	@NotNull
	@NotEmpty
	protected Set<Company> companySet = new HashSet<Company>();

	public Set<Company> getCompanySet() {

		if (this.companySet == null)
			return Collections.unmodifiableSet(new HashSet<Company>());

		return Collections.unmodifiableSet(this.companySet);
	}

	public void addCompany(Company company) {

		if (company == null)
			return;

		if (this.companySet == null)
			this.companySet = new HashSet<Company>();

		this.companySet.add(company);
	}
}
