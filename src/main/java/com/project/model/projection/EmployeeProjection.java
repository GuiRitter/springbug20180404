package com.project.model.projection;

import org.springframework.data.rest.core.config.Projection;

import com.project.model.Employee;

@Projection(name = "employeeProjection", types = { Employee.class }) 
public interface EmployeeProjection {

    Boolean getIsDeleted();

    Boolean getIsEnabled();

    String getCode();

    String getName();

    /*
     * ...
     */
}