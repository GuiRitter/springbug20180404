package com.project.model.projection;

import java.util.List;

import org.springframework.data.rest.core.config.Projection;

import com.project.model.ProductGroup;

@Projection(name = "productGroupProjection", types = { ProductGroup.class }) 
public interface ProductGroupProjection {
	
	Boolean getIsDeleted();
	
	Boolean getIsEnabled();

	String getCode();

	String getName();

	ProductGroup getParentGroup();

	List<ProductGroup> getSubgroupList();
}
