package com.project.model.projection;

import javax.persistence.ManyToOne;

import org.springframework.data.rest.core.config.Projection;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.project.model.ItemVenda;

@Projection(name = "saleItemProjection", types = { SaleItem.class })
//@JsonIdentityInfo(
//		  generator = ObjectIdGenerators.PropertyGenerator.class,
//		  property = "id",
//		  scope = SaleItemProjection.class)
public interface SaleItemProjection {

	Boolean getIsDeleted();

	Boolean getIsEnabled();

	@JsonBackReference
	@ManyToOne
	SaleProjection getSale();
}
