package com.project.model.projection;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;

import org.springframework.data.rest.core.config.Projection;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.project.model.Sale;

@Projection(name = "saleProjection", types = { Sale.class })
//@JsonIdentityInfo(
//		  generator = ObjectIdGenerators.PropertyGenerator.class,
//		  property = "id",
//		  scope = SaleProjection.class)
public interface SaleProjection {

	Boolean getIsDeleted();

	Boolean getIsEnabled();

	CompanyProjection getCompany();

	// ...

	@JsonManagedReference
	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy="sale")
	List<SaleItemProjection> getSaleItemList();

	// ...
}
