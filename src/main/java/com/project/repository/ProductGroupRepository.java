package com.project.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.project.model.ProductGroup;
import com.project.model.projection.ProductGroupProjection;
import com.project.repository.custom.ProductGroupRepositoryCustom;

@RepositoryRestResource(collectionResourceRel = "productGroup", path = "productGroups", excerptProjection = ProductGroupProjection.class)
public interface ProductGroupRepository extends PagingAndSortingRepository<ProductGroup, Long>, ProductGroupRepositoryCustom, JpaSpecificationExecutor<ProductGroup> {}
