package com.project.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.project.model.SaleItem;
import com.project.model.projection.SaleItemProjection;

@RepositoryRestResource(collectionResourceRel = "saleItem", path = "saleItems", excerptProjection = SaleItemProjection.class)
public interface SaleItemRepository extends PagingAndSortingRepository<SaleItem, Long>, JpaSpecificationExecutor<SaleItem> {}
