package com.project.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.project.model.Sale;
import com.project.repository.custom.SaleRepositoryCustom;

@RepositoryRestResource(collectionResourceRel = "sale", path = "sales")
public interface SaleRepository extends PagingAndSortingRepository<Sale, Long>, SaleRepositoryCustom {}
