package com.project.repository.custom;

import org.springframework.data.repository.query.Param;

import com.project.Employee;

public interface EmployeeRepositoryCustom {

    <S extends Employee> S save(S entity);
}
