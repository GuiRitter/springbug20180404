package com.project.repository.custom;

import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestBody;

import com.project.model.Employee;

public class EmployeeRepositoryCustomImpl implements EmployeeRepositoryCustom {

    @Override
    @Transactional
    public <S extends Employee> S save(@RequestBody S entity) {
        /*
         * ...
         */
        return entity;
    }
}
