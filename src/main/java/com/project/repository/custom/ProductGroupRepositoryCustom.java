package com.project.repository.custom;

import java.util.List;

import org.springframework.data.repository.query.Param;

import com.project.model.ProductGroup;

public interface ProductGroupRepositoryCustom {

	List<ProductGroup> find…(@Param("company") Long company);

	<S extends ProductGroup> S save(S entity);

	<S extends ProductGroup> Iterable<S> saveAll(Iterable<S> entities);
}
