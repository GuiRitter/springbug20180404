package com.project.repository.custom;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.util.Assert;

import com.project.model.ProductGroup;

@Transactional
@SuppressWarnings("unchecked")
public class ProductGroupRepositoryCustomImpl implements ProductGroupRepositoryCustom {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<ProductGroup> find…(Long company) {
		// ...
	}

	@Override
	public <S extends ProductGroup> S save(S entity) {
		// ...
	}

	@Override
	public <S extends ProductGroup> Iterable<S> saveAll(Iterable<S> entities) {
		// ...
	}
}
