package com.project.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.project.model.SaleItem;

public class SaleItemValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return SaleItem.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {

		System.err.println(getClass().getSimpleName() + " validate");
	}
}
